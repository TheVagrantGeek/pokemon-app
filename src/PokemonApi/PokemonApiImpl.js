import PokemonApi from "./PokemonApi";

class PokemonApiImpl extends PokemonApi {
  _createPokedex() {
    const Pokedex = require("pokeapi-js-wrapper");
    const P = new Pokedex.Pokedex();
    return P;
  }

  getAllPokemons(callback) {
    this._createPokedex()
      .getPokemonsList()
      .then(r =>
        r.results.map(x => {
          return { nom: x.name };
        })
      )
      .then(r => callback(r));
  }

  getPokemonByName(name, callback) {
    this._createPokedex()
      .getPokemonByName(name)
      .then(x => {
        console.log(x);
        return {
          name: x.name,
          image : x.sprites.front_default
        };
      })
      .then(r => callback(r));
  }

  getTypes(callback) {
    this._createPokedex()
      .getTypesList()
      .then(x =>
        x.results.map(y => {
          return {
            name: y.name
          };
        })
      )
      .then(r => callback(r));
  }
}

export default PokemonApiImpl;
