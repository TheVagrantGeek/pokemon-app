import React from "react";

import "bootstrap/dist/css/bootstrap.min.css";

//Todo. Trouver une façon de référer un module sans référer un path?
import PokemonViewer from "./Components/Pokemon/PokemonViewer"

function App() {
  return (
    <div className="App">
      <PokemonViewer />
    </div>
  );
}

export default App;
