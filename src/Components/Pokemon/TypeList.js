import React from 'react'

class TypeList extends React.Component
{
    render(){
        return (
            <select>
                <option value=""></option>
                {this.props.types.map(t => (
                    <option key={t.name} value={t.name}>{t.name}</option>
                ))}
            </select>
        )
    }
}

export default TypeList