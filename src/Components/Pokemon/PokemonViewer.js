import React from "react";

import TypeList from "./TypeList";
import PokemonList from "./PokemonList";
import PokemonDetails from "./PokemonDetails";

import PokemonApi from "../../PokemonApi/PokemonApiImpl";

class PokemonViewer extends React.Component {
  constructor(props) {
    super(props);

    this.api = new PokemonApi();

    this.state = {
      detailsAreShown: false,
      pkList: [],
      types: [],
      shownPokemon: {}
    };

    this.fetchTypes();
    this.getPokemonList();
  }

  getPokemonList() {
    this.api.getAllPokemons(r => {
      this.setState({ pkList: r });
    });
  }

  fetchTypes() {
    this.api.getTypes(r => {
      this.setState({ types: r });
    });
  }

  showPokemonDetails(name) {
    new PokemonApi().getPokemonByName(name, r => {
      this.setState({
        detailsAreShown: true,
        shownPokemon: r
      });
    });
  }

  showList() {
    this.setState({
      detailsAreShown: false,
      shownPokemon: {}
    });
  }

  render() {
    return (
      <div className="pokemonViewer">
        <h1>PokemonViewer</h1>
        {!this.state.detailsAreShown ? (
          <div>
            <TypeList types={this.state.types} />
            <PokemonList
              pk={this.state.pkList}
              onClick={name => this.showPokemonDetails(name)}
            />
          </div>
        ) : (
          <PokemonDetails
            pk={this.state.shownPokemon}
            onBack={() => this.showList()}
          />
        )}
      </div>
    );
  }
}

export default PokemonViewer;
