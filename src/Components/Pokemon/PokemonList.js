import React from "react";

import PokemonListItem from "./PokemonListItem";

class PokemonList extends React.Component {

  render() {
    return (
      <div>
        <div>PokemonList</div>
        {this.props.pk.map(i => (
          <PokemonListItem key={i.nom} name={i.nom} onClick={(name) => this.props.onClick(name)} />
        ))}
      </div>
    );
  }
}

export default PokemonList;
