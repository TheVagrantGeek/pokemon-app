import React from 'react'

class PokemonDetails extends React.Component
{
    render(){
        return (
            <div>
                <h2>Who is this pokemon?</h2>
                <h3>{this.props.pk.name}</h3>
                <img src={this.props.pk.image} alt={this.props.pk.name}/>
                <button onClick={() => this.props.onBack()}>Back</button>
            </div>
        )
    }
}

export default PokemonDetails