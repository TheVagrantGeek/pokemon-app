import React from "react";
// import PropTypes from "prop-types";

class PokemonListItem extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e){
    console.log(this);
  }
  
  render() {
    return (
      <div>
        <a href="#" onClick={(e) => {e.preventDefault(); this.props.onClick(this.props.name);}}>PokemonListItem {this.props.name}</a>
      </div>
    );
  }
}

export default PokemonListItem;
